# RESTful MircoPython - a REST micro framework for microcontrollers

The goal of RESTful MicroPython is it to bring the simplicity and the power of a microframework like Flask to a micro controller like the ESP8266.
I like the power and the possibilities of the really cheap ESP8266 controller, but in my opinion the arduino code for a simple http interface looks mostly messy. 
Looking around for better solutions I found the great [MicroPython](https://micropython.org/) projekt. But I want to program the ESP easy like [Flask](http://flask.pocoo.org/). So the idea for this project was born.
Programming a SmartHome solution with 15 lines of code ;)

## Using RESTful MicroPython
### Hello World 
```python 
import rest_interface

rest = rest_interface.Connection()

@rest.route('/say_hello')
def hello():
    return "Hello World"

rest.app()
```

### Blink
```python
from machine import Pin

import rest_interface

rest = rest_interface.Connection()
led = Pin(2, Pin.OUT)

@rest.route('/toggle_led')
def blink():
    led.value(not led.value())
    return "LED: {0}".format(led.value())

rest.app()
```

### Build a simple temperature measurment server using a DS18B20 temp Sensor
```python
from time import sleep

from machine import Pin
import onewire
import ds18x20

import rest_interface

rest = rest_interface.Connection()
temp_sensor = ds18x20.DS18X20(onewire.OneWire(Pin(4)))

@rest.route('/temp')
def get_temperature()
    data = temp_sensor.scan()
    temp_sensor.convert_temp()
    sleep(0.7)
    temp = temp_sensor.read_temp(data[0])
    return "we have a temperature of {0} °C".format(temp)

rest.app()
```