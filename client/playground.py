import re

string1 = "/infodsakdsad?affe=255&tee=3&rot=23"
string2 = "http://www.test.de?"


regex = re.compile(r'(\?|&)([^=]+)=([^&]+)')

print(regex.search(string1).group(0))
print(regex.split(string1))

k = string1.split("?")[1].split("&")
k = [i.split("=") for i in k]
d = {key: value for (key, value) in k}
print(d)
