# -*- coding: utf-8 -*-
# Copyright (C) 2016 Stefan Schmidt <info@schmidtstefan.com>

"""boot.py: will be executed will starting ESP"""

import webrepl
import esp
import wifi

webrepl.start()
esp.osdebug(None)
