# -*- coding: utf-8 -*-
# Copyright (C) 2016 Stefan Schmidt <info@schmidtstefan.com>

"""rest_interface.py: a simple REST Interface (Flask inspired) for the ESP using micro Python"""

import socket
import json


class Connection:
    """This class represents a singe socket on a specific port"""

    def __init__(self, address='0.0.0.0', port=80):
        self.url_rules = {'/info': (self.get_function_info, {})}
        self.server_socket = socket.socket()
        self.server_socket.bind((address, port))
        self.server_socket.listen(1)
        self.client_socket = None

    def app(self):
        """a endless loop that take a request and send a response over http"""
        while True:
            requested_method, requested_parameter = self.get_client_request()
            print(requested_method, requested_parameter)
            if requested_method in self.url_rules:
                method_to_call = self.url_rules[requested_method][0]
                type_parameter_to_call = self.url_rules[requested_method][1]
                print(type_parameter_to_call)
            else:
                self.send_response(405, 'text')
                continue
            if len(type_parameter_to_call) < len(requested_parameter):
                self.send_response(405, 'text', 'to many parameters')
                continue
            if requested_parameter:
                for key in requested_parameter.keys():
                    try:
                        requested_parameter[key] = type_parameter_to_call[key](requested_parameter[key])
                    except ValueError:
                        self.send_response(405, 'text', 'parameter {0} have a wrong type'.format(key))
                        continue
                    except KeyError:
                        # self.send_response(400, 'text', 'unknown parameter')
                        # TODO: at this moment a wrong parameter will discard
                        continue
                print(requested_parameter)
            try:
                response = method_to_call(**requested_parameter)
                print(response)
                # response = self.url_rules[client_request[0]](**client_request[1])
                self.make_response(response)
            except TypeError:
                self.send_response(400, 'text', 'bad parameter')

    def route(self, url, **kwargs):
        """A wrapper to register specific user functions for the interface"""
        def decorator(func):
            self.add_url_rule(url, (func, kwargs))
            return func
        return decorator

    def add_url_rule(self, url, rule):
        self.url_rules[url] = rule

    def get_function_info(self):
        return self.url_rules

    @staticmethod
    def parse_request(request):
        """a really bad parser, need to improve"""
        print(request)
        request = request[0].format('utf-8').lower().split(" ")[1]
        try:
            command, parameters = request.split('?')
            parameters = parameters.split("&")
            parameters = [i.split("=") for i in parameters]
            parameters = {key: value for (key, value) in parameters}
            print(parameters)
        except ValueError:
            command = request
            parameters = {}

        return command, parameters

    def get_client_request(self):
        """process the user input and returns the request and the parameters"""
        self.client_socket, client_address = self.server_socket.accept()
        print('client connected from', client_address)

        client_content = []
        buf_content = self.client_socket.makefile('rwb', 0)
        while True:                                         # TODO: timeout to beware of a endless loop
            line = buf_content.readline()
            client_content.append(line)
            if not line or line == b'\r\n':
                break
        if not client_content:
            return
        else:
            return self.parse_request(client_content)

    def send_response(self, statuscode, content_type, message=""):
        """Build the http response and send it to the client"""
        code = {102: 'Processing',
                200: 'OK',
                204: 'No Content',
                400: 'Bad Request',
                404: 'Not Found',
                405: "Method Not Allowed"}
        content = {'json': 'application/json',
                   'text': 'text/plain',
                   'html': 'text/html'}

        try:                                                    # TODO: OSERROR EBADF abfangen
            self.client_socket.send('HTTP/1.1 {0} {1}\r\n'
                                    'Content-Type: {2}; charset=utf-8'
                                    '\r\n\r\n'
                                    '{3}'
                                    .format(str(statuscode), code[statuscode], content[content_type], message))
            self.client_socket.close()
        except KeyError:
            raise KeyError('possible http-codes: {0}, possible content_type: {1}'
                           .format([key for key in code], [key for key in content]))

    def make_response(self, return_value, force_text_to_html=False):
        """Calls send_response and choose the content-Type and http-code"""
        if return_value is None:
            raise ValueError('Function do not return a value')

        elif isinstance(return_value, dict):
            self.send_response(200, 'json', json.dumps(return_value))
        elif isinstance(return_value, str):
            if force_text_to_html:
                self.send_response(200, 'html', '<html>'
                                                '<head>'
                                                '<title>ESP Control</title>'
                                                '</head>'
                                                '<body>{0}</body>'
                                                '</html>'.format(return_value))
            else:
                self.send_response(200, 'text', return_value)

        else:
            raise ValueError('datatype {0} not supported'.format(type(return_value)))
