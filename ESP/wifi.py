# -*- coding: utf-8 -*-
# Copyright (C) 2016 Stefan Schmidt <info@schmidtstefan.com>

"""a helper file to simplicity the using of the wifi interface
"""
import json

import network

access_point = network.WLAN(network.AP_IF)
client = network.WLAN(network.STA_IF)
connection_data = {"ssid": "", "password": ""}


def auto_connect():
    """checks the lan.json file for known wifi connections and connect to the first"""
    client.active(True)

    with open('lan.json') as data_file:
        keys = json.loads(data_file.read())

    for ssid in client.scan():
        ssid = ssid[0].format("utf-8")
        if ssid in keys.keys():
            print("connecting to {0}".format(ssid))
            connection_data["ssid"] = ssid
            client.connect(ssid, keys[ssid])


def connect(ssid="", key=""):
    """scans for wifi networks and ask the user for the network and the password"""
    if not ssid:
        networks = scan()
        try:
            selection = int(input("choose a wifi: "))
            ssid = networks[selection][0]
        except ValueError:
            print("really? choose a number!")
            return

    if not key:
        key = input("password: ")
    connection_data["ssid"] = ssid
    connection_data["password"] = key
    client.connect(ssid, key)


def start_ap(name="ESP-AP"):
    """starts a access point"""
    access_point.active(True)
    access_point.config(essid=name)


def stop_ap():
    """stops the access point"""
    access_point.active(False)


def scan():
    """scans available networks and prints the info"""
    networks = client.scan()
    auth_mode = ["open", "WEP", "WPA-PSK", "WPA2-PSK", "WPA/WPA2-PSK"]

    for i, lan in enumerate(networks):
        print("{0:>3}  {1:<25} Channel: {2:2}  Signal {3:4} dB  AUTH: {4}"
              .format(i, lan[0][:25], lan[2], lan[3], auth_mode[lan[4]]))

    return networks


def info():
    """"prints the actually status of the connection"""
    status = ("no connection and no activity",
              "connecting in progress",
              "failed due to incorrect password",
              "failed because no access point replied",
              "failed due to other problems",
              "connection successful")
    config_data = client.ifconfig()

    print("SSID:            {0}\n"
          "IP address:      {1}\n"
          "IP access point: {2}\n"
          "status:          {3}"
          .format(connection_data["ssid"], config_data[0], config_data[2], status[client.status()]))
