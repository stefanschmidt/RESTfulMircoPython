# -*- coding: utf-8 -*-
# Copyright (C) 2016 Stefan Schmidt <info@schmidtstefan.com>

"""main.py: a sample program for the ESP using the REST interface"""

from time import sleep

from machine import Pin
from neopixel import NeoPixel
import onewire
import ds18x20

import rest_interface

led = Pin(2, Pin.OUT)
np = NeoPixel(Pin(5), 60)
np.fill((0, 0, 0))
np.write()

rest = rest_interface.Connection()


@rest.route('/hello')
def hello():
    return "Hello World"


@rest.route('/toggle_led')
def test_function():
    led.value(not led.value())
    return "LED: {0}".format(led.value())


@rest.route('/neopixel', red=int, green=int, blue=int)
def power_the_stripe(red=0, green=0, blue=0):
    np.fill((int(red), int(green), int(blue)))
    np.write()
    return "Stripe is on"


@rest.route('/temp')
def get_temperature():
    data_pin = Pin(3, Pin.OUT)
    ds = ds18x20.DS18X20(onewire.OneWire(data_pin))
    data = ds.scan()
    ds.convert_temp()
    sleep(0.7)
    temp = ds.read_temp(data[0])
    return "{0} °C".format(temp)

rest.app()
